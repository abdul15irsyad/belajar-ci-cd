import express, { Express, Request, Response } from 'express';
import dotenv from 'dotenv';

dotenv.config();
const app: Express = express();
const port = process.env.PORT || 9000;

app.get('/', (req: Request, res: Response) => {
  return res.status(200).json({
    message: 'test ci/cd gitlab cuy',
  });
});

app.listen(port, () => console.log(`running on port ${port}`));
